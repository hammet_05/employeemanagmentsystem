package com.spartans.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.spartans.exceptions.ResourceNotFound;
import com.spartans.model.Employee;
import com.spartans.repository.EmployeeRepository;

@RestController
@RequestMapping("/api/")
public class EmployeeController
{
	@Autowired
	private EmployeeRepository employeeRepository;
	
	@GetMapping("/employees")
	public List<Employee>GetAllEmployees()
	{
		return employeeRepository.findAll();
	}
	@GetMapping("/employees/{id}")
	public ResponseEntity<Employee> GetEmployeeById(@PathVariable(value="id") Long employeeId)
	throws ResourceNotFound
	{
		Employee employee=employeeRepository.findById(employeeId)
				.orElseThrow(()->new ResourceNotFound("Employee not found for this::" + employeeId));
		return ResponseEntity.ok().body(employee);
	}
	
}
